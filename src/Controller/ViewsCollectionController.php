<?php

namespace Drupal\views_collection\Controller;

use Drupal\Component\Serialization\Json;
use \Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\views\ViewExecutable;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * An example controller.
 */
class ViewsCollectionController extends ControllerBase {

  /**
   * The entity type manager instance.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Constructs a new ViewsCollectionController instance.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * Returns a renderable array for a test page.
   *
   * return []
   */
  public function title($tag = '') {
    if (empty($tag)) {
      return $this->t('Untagged views');
    }

    return $this->t('Views tagged %tag', ['%tag' => $tag]);
  }



  /**
   * Returns a renderable array for a test page.
   *
   * return []
   */
  public function untagged($tag = '') {

    $views = $this->entityTypeManager->getStorage('view')->loadByProperties([
      'tag' => '',
    ]);

    return $this->buildViewsTable($views);
  }

  /**
   * Returns a renderable array for a test page.
   *
   * return []
   */
  public function tags($tag) {
    $view_ids = \Drupal::entityQuery('view')->condition('tag', $tag, 'CONTAINS')->execute();
    $views = $this->entityTypeManager->getStorage('view')->loadMultiple($view_ids);
    return $this->buildViewsTable($views);
  }

  protected function buildViewsTable($views) {
    $rows = [];

    foreach ($views as $delta => $view) {
      $cols = [
        ['data' => $view->label()],
        ['data' => $view->id()],
        ['data' =>
          [
            '#type' => 'operations',
            '#links' => $this->getOperations($view),
            '#attached' => [
              'library' => ['core/drupal.dialog.ajax'],
            ],
          ],
        ],
      ];

      $rows[$delta] = $cols;
    }

    $output['table'] = [
      '#theme' => 'table',
      '#header' => [$this->t('View name'),$this->t('Machine name'),  $this->t('Operations')],
      '#rows' => $rows,
    ];

    return $output;
  }

  public function getOperations(EntityInterface $entity) {
    $operations = $this->getDefaultOperations($entity);
    $operations += $this->moduleHandler()->invokeAll('entity_operation', [$entity]);
    $this->moduleHandler->alter('entity_operation', $operations, $entity);
    uasort($operations, '\Drupal\Component\Utility\SortArray::sortByWeightElement');

    return $operations;
  }

  protected function getDefaultOperations(EntityInterface $entity) {
    $operations = [];
    if ($entity->access('update') && $entity->hasLinkTemplate('edit-form')) {
      $operations['edit'] = [
        'title' => $this->t('Edit'),
        'weight' => 10,
        'url' => $entity->toUrl('edit-form'),
      ];
    }
    if ($entity->access('delete') && $entity->hasLinkTemplate('delete-form')) {
      $operations['delete'] = [
        'title' => $this->t('Delete'),
        'weight' => 100,
        'attributes' => [
          'class' => ['use-ajax'],
          'data-dialog-type' => 'modal',
          'data-dialog-options' => Json::encode([
            'width' => 880,
          ]),
        ],
        'url' => $entity->toUrl('delete-form'),
      ];
    }

    return $operations;
  }


}
